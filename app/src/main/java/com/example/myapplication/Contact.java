package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Contact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Button f = findViewById(R.id.btnBack);
        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent g = new Intent(Contact.this,MainActivity.class);
                startActivity(g);
            }
        });
    }
    public void imageView5 (View v){
        Intent h = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/profile.php?id=100008068354176"));
        startActivity(h);
    }

    public void Click_ig(View view) {
        Intent intent1 = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.instagram.com/zirachat/"));
        startActivity(intent1);
    }

    public void Click_yt(View view) {
        Intent intent2 = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/channel/UCUiNLqvtKGzHUOfR04o374A?view_as=subscriber"));
        startActivity(intent2);
    }
}